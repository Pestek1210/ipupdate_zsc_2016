<?php
//	### INFO ###
//	# Copyright 2016 by Jakub Pestka.
//	# All rights reserved.
//	### ENDINFO ###

	// wyswietlanie bledow
	ini_set( 'display_errors', 'On' );
	error_reporting( E_ALL );

	// przygotowanie danych, czytanie configu
	$config = file_get_contents("config.json");
	$config = json_decode($config);
	$email = $config->email;
	$api = $config->api;
	$zone = $config ->zone;
	$record = $config->record;

	$record_name = str_replace('.'.$zone.'', '', $record);
	$record_name = trim($record_name);

	// pobieranie id rekordu
	$check_rec = 'https://www.cloudflare.com/api_json.html?a=rec_load_all&tkn='.$api.'&email='.$email.'&z='.$zone.'';
	$check_rec = file_get_contents($check_rec);
	$check_rec = json_decode($check_rec);

	foreach($check_rec->response->recs->objs as $item)
	{
		if($item->name == $record)
		{
			$rec_id = $item->rec_id;
		}
	}

	// sprawdzanie adresu ip
	$check_ip_url = file_get_contents('http://216.146.38.70/');
	$check_ip = str_replace('<html><head><title>Current IP Check</title></head><body>Current IP Address: ', '', $check_ip_url);
	$check_ip = str_replace('</body></html>', '', $check_ip);
	$check_ip = trim($check_ip);
	$ip = $check_ip;

	// czytanie ostatniego ip
	$fp = fopen("ip.txt", "r");
	$oldip = fread($fp, filesize("ip.txt"));
	fclose($fp);

	// sprawdzanie czy adres ulegl zmianie
	if ($oldip != $ip) {
		// aktualizacja adresu ip w dns
		$update_ip_url = 'https://www.cloudflare.com/api_json.html?a=rec_edit&type=A&ttl=1&name='.$record_name.'&z='.$zone.'&id='.$rec_id.'&email='.$email.'&tkn='.$api.'&content='.$ip.'';
		file_get_contents($update_ip_url);

		// zapis nowego ip w pliku
		$fp = fopen("ip.txt", "w");
		flock($fp, LOCK_EX);
		fputs($fp, $ip);
		flock($fp, LOCK_UN);
		fclose($fp);
	}
?>
