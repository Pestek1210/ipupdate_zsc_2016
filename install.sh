#!/bin/bash
DIR=$(pwd)
useradd -M -d $DIR -u 591 -s /bin/bash ipupdate
chmod 700 $DIR
chmod 600 $DIR/update.php
chmod 600 $DIR/config.json
chmod 600 $DIR/ip.txt
echo "*/5 *    * * *   ipupdate    cd $DIR && php $DIR/update.php" >> /etc/crontab
